This is a bunch of shell scripts and Windows+Linux programs to manipulate time.
See  http://www.gdargaud.net/Hack/SourceCode.html#StrfTime  

Most use the following additional specifiers:

%n	Decimal minutes,           59.983333  
%h	Decimal hour,              23.999722  
%D	Fraction of day,           0.999988  
%J	Decimal julian day,        364.999988  
%q	fractional year,           0.9999999682191	(warning, non linear)  
%Q	Decimal year,              1999.9999999682191	(warning, non linear)  
%o	Letter month, lowercase	   l  
%O	Letter month, uppercase	   L  
%1	hours since start of year  8759  
%2	hours since start of month 743  
%3	min since start of year    525599  
%4	min since start of month   44639  
%5	min since start of day     1439  
%6	sec since start of year    31535999  
%7	sec since start of month   2678399  
%8	sec since start of day     86399  
%9	sec since start of hour    3599  
%t	time_t internal (compiler specific, number of seconds since 1900 or 1970 or 1904...)  3155666399

TODO: if a register_strftime_function is ever implemented, rewrite with it.

Also:

PROGRAM:   RollLog.exe                                                          
VERSION:   1.0 2001  
AUTHOR:    Guillaume Dargaud - Freeware  
PURPOSE:   Roll a file by creating a copy or renaming it to File_Date.  
INSTALL:   Extract the prog from the zip file.  
           If setup fails, you may need to install the LabWindows/CVI runtime engine on your PC.  
           It is available at http://www.ni.com/  
HELP:      Run the program from a DOS shell without argument.  
           Use is very simple, simply drag and drop onto the program the files you want to rename  
TUTORIAL:  http://www.gdargaud.net/Hack/Utils.html#RollLog  
LICENSE:   GPLv3
