#! /bin/ksh
# Guillaume Dargaud 2001/11/14

######## Usage info ##########
if [ $# -le 2 ]; 
then
	echo 1>&2 "Usage: $0 StartTime EndTime \"Commands\""
	echo 1>&2 "Loop a time/date between two dates and executes the command on each"
	echo 1>&2 "Use @@ to get the time in the Command"
	echo 1>&2 "Times are specified using YY[MM[DD[HH[MM[SS]]]]], EndTime is excluded"
	exit 127
fi

################################################################################
#PATH=$PATH:/Users/dargaud/bin:/usr/local/bin:$DirExe; export PATH

Time=$1
Stop=$2
shift 2

while [ $Time != $Stop ]
do
	eval `echo $@ | sed "s/@@/$Time/g"`
	Time=`PreviousTime.exe $Time -n`
done
