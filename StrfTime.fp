s��         -     (�  P   �   ����                                                               Extra Time Related Functions                                                                             � ��time_t  � � ��size_t     	� 	��struct tm     � ��struct tm *  %    Time related functions, in particular:

- Extension of the C strftime function with more options.
This functions behaves otherwise like the <time.h> strftime function.

- Format ta time difference (ie: 5 days 2 hours)

- Julian day conversions

- Lenght of years and months


There are 3 command line programs based on this library: PreviousTime.exe, FormatTime.exe and JulianDate.exe


This code was originally intended for LabWindows/CVI, but it's ANSI C and works in Unix, Windows...

(c) Guillaume Dargaud 1997-2002
Free distribution and use.
    '    Format a single date/time into a string
Add 18 new formatting codes to the strftime ANSI C function.
The use is exactly the same.

Example of use:
void main(void) {
    time_t Time=time(NULL);
    struct tm *TM=localtime(&Time);
    #define DIM 500
    char ST[DIM];
    int Len;

    Len=StrfTime(ST, DIM, "%y%o%d %Hh%M.tfc", TM);                   printf("%s\n", ST);
    Len=StrfTime(ST, DIM, "Normal: %a %b %d %H:%M:%S %Y", TM);       printf("%s\n", ST);
    Len=StrfTime(ST, DIM, "%%n: Decimal minutes,           %n", TM); printf("%s\n", ST);
}     �    The maximum number of characters to place into s String, including the terminating ASCII NUL byte.

The length of the string is returned by the function.    �    Additional formatting codes
(example given for Fri Dec 31 23:59:59 1999)

    %n  Decimal minutes,           59.983333
    %h  Decimal hour,              23.999722
    %D  Fraction of day,           0.999988
    %J  Decimal julian day,        364.999988
    %q  fractional year,           0.9999999682191      (warning, non linear)
    %Q  Decimal year,              1999.9999999682191   (warning, non linear)
    %o  Letter month, lowercase    l
    %O  Letter month, uppercase    L
    %1  hours since start of year  8759
    %2  hours since start of month 743
    %3  min since start of year    525599
    %4  min since start of month   44639
    %5  min since start of day     1439
    %6  sec since start of year    31535999
    %7  sec since start of month   2678399
    %8  sec since start of day     86399
    %9  sec since start of hour    3599
    %t  time_t internal (compiler specific, number of seconds since 1900 or 1970)  3155666399
  
Normal ANSI C formatting codes:
    %a  abbreviated weekday name    "Mon"...
    %A  full weekday name           "Monday"...
    %b  abbreviated month name      "Jan"...
    %B  full month name             "January"...
    %c  locale-specific date and time
    %d  day of the month as integer 01-31
    %H  hour (24-hour clock)        00-23
    %I  hour (12-hour clock)        01-12
    %j  day of the year as integer  001-366
    %m  month as integer            01-12
    %M  minute as integer           00-59
    %p  locale AM/PM designation    
    %S  second as integer           00-61 (allow for up to 2 leap seconds)
    %U  week number of the year     00-53 (week number 1 has 1st sunday)
    %w  weekday as integer          (0-6, sunday=0)
    %W  week number of the year     00-53 (week number 1 has 1st monday)
    %x  locale specific date
    %X  locale specific time
    %y  year without century        00-99
    %Y  year with century           1900...
    %z  time zone name
    %%  a single %%
     a    Time structure pointer.
example: 
   time_t Time=time(NULL);
   struct tm* TM=localtime(&Time);     d    Output string where the result is formated.

The length of the string is returned by the function.    x $ � �       MaxSize                            j          Format                          ���� �����       Length                            � j � �       TimePtr                           5 $           Output string                      0    ""    	            tm    	           $    Format the difference of two single dates/times into a string
This kind of formatting is absent from standard ANSI C, so I just made up some formatting codes with 4 basic options on what the needs might be:
- a normal number, IE 0..59 for minutes
- a total number: 48 hours or 2 days
- a zero filled number: 00:01:06 looks better than 0:1:6
- optionals: "1 minute 10 seconds" instead of "0 days 0 hours 1 minute 10 seconds"

Example of use:
int main (int argc, char *argv[]) {
    time_t Time=time(NULL), Time1=Time-1000000, Time2=Time+1000, Time3=Time-8;
    #define DIM 500
    char ST[DIM];
    int Len;
    
    #define Do(TIME) \
        Len=StrfTimeDiff(ST, DIM, "%(days )D%(hours )h%(min )m%ssec", Time, TIME);              printf("\n%s\n", ST);\
        Len=StrfTimeDiff(ST, DIM, "%D days %0h:%0m:%0s", Time, TIME);                               printf("%s\n", ST);\
        Len=StrfTimeDiff(ST, DIM, "%Ss = %Mm %ss = %Hh %mm %ss = %Dd %hh %mm %ss", Time, TIME);  printf("%s\n", ST);
    
    Do(Time);
    Do(Time1);
    Do(Time2);
    Do(Time3);
}
     �    The maximum number of characters to place into s String, including the terminating ASCII NUL byte.

The length of the string is returned by the function.    	�    Additional formatting codes
  %D      Number of Days of difference 0...
  %()D    Optional number of Days of difference 1... (prints nothing if less than one days)
          In parenthesis, the text to be optionaly printed after
                
  %H      Total number of hours of difference 0...
  %h      Number of hours of difference  0..23
  %0h     Fills with 0 if less than 10: 00..23
  %()h    Optional number of Hours of difference 1..23 (prints nothing if less than one hour)
  %()H    Optional total number of Hours of difference 1..23 (prints nothing if less than one hour, or the number followed by the content of the parenthesis otherwise)
                
  %M      Total number of minutes of difference
  %m      Number of Minutes of difference 0..59
  %0m     Fills with 0 if less than 10:  00..59
  %()m    Optional number of Minutes of difference 1..59 (prints nothing if less than one minute)
  %()M    Optional number of Minutes of difference 1..59 (prints nothing if less than one minute, or the number followed by the content of the parenthesis otherwise)
                
  %S      Total number of seconds of difference
  %s      Number of Seconds of difference 0..59
  %0s     Fills with 0 if less than 10:  00..59
  %()s    Optional number of Seconds of difference 1..23 (prints nothing if less than one second)
  %()S    Optional Total number of Seconds of difference 1..23 (prints nothing if less than one second, or the number followed by the content of the parenthesis otherwise)

 %()+    Prints the content of the parenthesis if Time2 > Time1
 %()-    Prints the content of the parenthesis if Time2 < Time1"


EXAMPLES given for 0, 1000000, 1000 and 8 seconds of difference:

            "%(+)+%(days )D%(hours )h%(min )m%ssec"  
0           0sec                                
1000000     +11days 13hours 46min 40sec          
1000        +16min 40sec                         
8           +8sec                                

            "%(-)-%D days %0h:%0m:%0s"   
0           0 days 00:00:00         
1000000     11 days 13:46:40        
1000        0 days 00:16:40         
8           0 days 00:00:08         

            "%Ss = %Mm %ss = %Hh %mm %ss = %Dd %hh %mm %ss"        
0           0s = 0m 0s = 0h 0m 0s = 0d 0h 0m 0s                    
1000000     1000000s = 16666m 40s = 277h 46m 40s = 11d 13h 46m 40s 
1000        1000s = 16m 40s = 0h 16m 40s = 0d 0h 16m 40s           
8           8s = 0m 8s = 0h 0m 8s = 0d 0h 0m 8s                    

     �    This control contains the calendar time from which Time 2 will be subtracted.  A calendar time may be obtained by calling the time() or mktime() functions.     d    Output string where the result is formated.

The length of the string is returned by the function.     �    This control contains the calendar time value that is subtracted from Time 1.  A calendar time may be obtained by calling the time() or mktime() functions.    � $ � �       MaxSize                           � g          Format                          ���� �����       Length                            i g � �       Time1                              $           Output string                     z g" �       Time2                              0    ""    	            0    	            0    �    This MACRO returns the length of the year (365 or 366 days) taking into account leap years, secular non-leap years and quadri-secular leap years.

example:
1996: 366
1999: 365
1900: 365
2000: 366     !    Year in 4 digits (1999, not 99)          Days in the year (365 or 366).    "W $            Year                              "� ~���         Length                             0    	            =    Return the number of days in a given month (from 28 to 31).         Year in YY or YYYY format.         Month (from 1 to 12)     S    Number of days in the month (28 to 31) taking into account all leap years, Y2K...    #h $            Year                              #� $ �          Month                             #� ����         DaysInMonth                        2000    1    	            Y    Return the julian day of the year (1 to 366), taking into account all leap year, Y2K...         Year in YY or YYYY format.         Month (from 1 to 12)     &    Corresponding Julian day (1 to 366).     7    Day of the month (1 to 31), not checked for validity.    % ? )           Year                              %B ? �          Month                             %` ����         JulianDay                         %� ?J          Day                                2000    1    	            1    q    Return the day and month knowing the year and julian day (1 to 366), taking into account all leap years, Y2K...         Year in YY or YYYY format.     "    Julian day to convert (1 to 366)         Returned month (1 to 12)         Returned Day (1 to 31)    '8 ? )           Year                              '\ ? �          Julian                            '� � )          Month                             '� � �          Day                                2000    1    
        1    
        1 ����         I  �             K.        StrfTime                                                                                                                                ����         �                K.        StrfTimeDiff                                                                                                                            ����         !�  "�             K.        LengthOfYear                                                                                                                            ����         ##  $             K.        DaysInMonth                                                                                                                             ����         $�  %�             K.        JulianDay                                                                                                                               ����         &�  '�             K.        FromJulianDay                                                                                                                                                                                                                      �String Format Time                                                                   �String Format Time Difference                                                        �Length of year (days)                                                                �Length of month (days)                                                               �Julian Day to YYMMDD                                                                 �YYMMDD From Julian Day                                                          