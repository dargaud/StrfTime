/*******************************************************************************
  PROGRAM: RollLog.exe
  PURPOSE: Roll a file by creating a copy or renaming it to File_Date.
  COMMAND LINE: RollLog.exe Y|M|D|H|N|S M|C File [DestDir]
  AUTHOR:  Guillaume Dargaud
  HISTORY: Jan 2001 - First version
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "Def.h"

#ifndef MAX_DRIVENAME_LEN
#define MAX_DRIVENAME_LEN 10
#define MAX_DIRNAME_LEN 255
#define MAX_FILENAME_LEN 255
#define MAX_PATHNAME_LEN 255
#endif

/******************************************************************************
  FUNCTION: main
******************************************************************************/
int main (int argc, char *argv[]) {
	time_t Time=time(NULL);
	struct tm *TM=localtime(&Time);
	#define DIM 20
	char TT[DIM];
	int Len, Res;
	char Drive[MAX_DRIVENAME_LEN], Dir[MAX_DIRNAME_LEN], FileExt[MAX_FILENAME_LEN];
	char File[MAX_FILENAME_LEN], Ext[MAX_FILENAME_LEN], DestPath[MAX_PATHNAME_LEN];
	char *Dot;
	
	////////////////////// Reading Command line parameters ////////////////////// 
	if (argc<4) {
		fprintf(stderr, "%s", "RollLog.exe Y|M|D|H|N|S M|C File [DestDir]"
			"\nRoll a file by creating a copy or renaming it to File_Date."
			"\nExample: \"RollLog M C c:\\Bootlog.txt\" will create a copy c:\\Bootlog_YYMM.txt"
			"\nParameters:"
			"\n  Y|M|D|H|M|S for the date/time to write"
			"\n	 M|C for Move or copy"
			"\n	 FileName to move or copy"
			"\n	 Optional directory for the destination file"
		);
		return 1;
	}
	
	SplitPath (argv[3], Drive, Dir, FileExt);
	Dot=strrchr(FileExt, '.');
	if (Dot==NULL) { Ext[0]='\0'; strcpy(File, FileExt); }
	else { strcpy(Ext, Dot+1); *Dot='\0'; strcpy(File, FileExt); }
	
	if (argc==5) sprintf(DestPath, argv[4]);
	else sprintf(DestPath, "%s%s", Drive, Dir);
	Len=strlen(DestPath);
	if (DestPath[Len-1]!='\\') {
		DestPath[Len+1]='\0';
		DestPath[Len]='\\';
	}

	switch (toupper(argv[1][0])) {
		case 'Y':strftime(TT, DIM, "%y", TM); break;
		case 'M':strftime(TT, DIM, "%y%m", TM); break;
		case 'D':strftime(TT, DIM, "%y%m%d", TM); break;
		case 'H':strftime(TT, DIM, "%y%m%d%H", TM); break;
		case 'N':strftime(TT, DIM, "%y%m%d%H%M", TM); break;
		case 'S':strftime(TT, DIM, "%y%m%d%H%M%S", TM); break;
	}
	
	if (Dot==0) sprintf(DestPath, "%s%s_%s", DestPath, File, TT);
	else sprintf(DestPath, "%s%s_%s.%s", DestPath, File, TT, Ext);

	printf("%s", DestPath);
	
	if (toupper(argv[2][0])=='M') {
		Res = CopyFile(argv[3], DestPath);
		if (Res==0) Res=DeleteFile(argv[3]);
	}
	else Res = CopyFile(argv[3], DestPath);

	switch (Res) {
		case  0:  break;
		case -1:  fprintf(stdout, "File not found or directory in path not found.\n"); break;
        case -3:  fprintf(stdout, "General I/O error occurred.\n"); break;
        case -4:  fprintf(stdout, "Insufficient memory to complete operation.\n"); break;
        case -5:  fprintf(stdout, "Invalid path (for either of the file names).\n"); break;
        case -6:  fprintf(stdout, "Access denied.\n"); break;
        case -7:  fprintf(stdout, "Specified source path is a directory, not a file.\n"); break;
        case -8:  fprintf(stdout, "Disk is full.\n"); break;
        case -9:  fprintf(stdout, "New file already exists.\n"); break;
        default:  fprintf(stdout, "Error.\n"); break;
	}
	
	return Res;
}
